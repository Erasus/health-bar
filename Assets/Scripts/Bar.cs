﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Health))]
public class Bar : MonoBehaviour
{
    [SerializeField] private Slider _slider;
    [SerializeField] private float _speed;
    [SerializeField] private Image _fill;
    [SerializeField] private float _fillingSpeed;
    [SerializeField] private Color _unfilledColor;
    [SerializeField] private Color _filledColor;
    [SerializeField] private ParticleSystem _damageEffect;
    [SerializeField] private ParticleSystem _healEffect;

    private Health _health;
    private Color _targetColor;
    private float _targetProgress;

    public event UnityAction ValueChanged;

    private void Awake()
    {
        _health = GetComponent<Health>();
    }

    private void Start()
    {
        SetStartValue();
    }

    private void OnEnable()
    {
        _health.HealthChanged += OnHealthChanged;
    }
    private void OnDisable()
    {
        _health.HealthChanged -= OnHealthChanged;
    }

    private void OnHealthChanged(float healthValue)
    {
        StartCoroutine(ChangeSliderValue(healthValue));
    }

    private IEnumerator ChangeSliderValue(float healthValue)
    {
        while(_slider.value != healthValue)
        {
            ChangeColor();
            _slider.value = Mathf.MoveTowards(_slider.value, healthValue, _speed * Time.deltaTime);

            if (_slider.value > healthValue)
                _damageEffect.Play();

            else if (_slider.value < healthValue)
                _healEffect.Play();

            if (_slider.value == healthValue)
            {
                _damageEffect.Stop();
                _healEffect.Stop();
            }

            yield return null;
        }

        ValueChanged?.Invoke();
    }

    private void ChangeColor()
    {
        _targetProgress = _slider.value / _slider.maxValue;
        _targetColor = Color.Lerp(_unfilledColor, _filledColor, _targetProgress);
        _fill.color = Color.Lerp(_fill.color, _targetColor, _fillingSpeed);
    }

    private void SetStartValue()
    {
        _slider.maxValue = _health.MaxHealth;
        _slider.value = _health.CurrentHealthValue;
        _fill.color = Color.Lerp(_unfilledColor, _filledColor, _slider.value / _slider.maxValue);
    }
}
