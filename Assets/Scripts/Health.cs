﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Bar))]
public class Health : MonoBehaviour
{
    [SerializeField] private Button _damageButton;
    [SerializeField] private Button _healButton;
    [Range(0, 100)]
    [SerializeField] private float _maxHealth;

    private Bar _bar;
    private bool _canChangeValue = true;
    private float _damage = 10;
    private float _heal = 10;
    private float _currentHealthValue;

    public event UnityAction<float> HealthChanged;
    public float MaxHealth => _maxHealth;
    public float CurrentHealthValue => _currentHealthValue;

    private void Awake()
    {
        _currentHealthValue = _maxHealth;
        _bar = GetComponent<Bar>();
    }

    private void OnEnable()
    {
        _bar.ValueChanged += OnValueChanged;
        _damageButton.onClick.AddListener(OnDamaged);
        _healButton.onClick.AddListener(OnHealed);
    }

    private void OnDisable()
    {
        _bar.ValueChanged -= OnValueChanged;
        _damageButton.onClick.RemoveListener(OnDamaged);
        _healButton.onClick.RemoveListener(OnHealed);
    }

    private void OnDamaged()
    {
        if (_currentHealthValue != 0 && _canChangeValue)
        {
            _currentHealthValue -= _damage;
            _canChangeValue = false;
            HealthChanged?.Invoke(_currentHealthValue);
        }
    }

    private void OnHealed()
    {
        if (_currentHealthValue != _maxHealth && _canChangeValue)
        {
            _currentHealthValue += _heal;
            _canChangeValue = false;
            HealthChanged?.Invoke(_currentHealthValue);
        }
    }

    private void OnValueChanged()
    {
        _canChangeValue = true;
    }
}
